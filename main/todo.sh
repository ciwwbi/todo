#!/bin/bash

#
# Error code :
# 0 => Ok, returns a result (can be empty)
# 1 => Param bad value or missing
# 2 => Internal problem
#

SCRIPT_NAME=`basename $0`
DATABASE=
ACTION=
ACTION_PARAM=
MAX_RETURN_LINES=-1
FILTER_BY_TAG=0

SCRIPT_DIRECTORY=`dirname $0`
source "${SCRIPT_DIRECTORY}/lib_message"
source "${SCRIPT_DIRECTORY}/lib_check"
source "${SCRIPT_DIRECTORY}/lib_actions"
source "${SCRIPT_DIRECTORY}/lib_filters"

read_options_and_parameters() {
  OPT="$(getopt -o "hHd:n:s:t:" -n "${SCRIPT_NAME}" -- "$@")"
  [ $? -eq 0 ] || { print_try_help ; exit 1 ; }

  eval set -- "$OPT"

  while true
  do
    case "$1" in
      -h|-H) 
        shift 1
        { usage ; exit 0; }
        ;;
      -d)
        DATABASE="$2" 
        validate_reading_db "$DATABASE" || exit_error 1
        shift 2
        ;;
      -n)
        MAX_RETURN_LINES="$2"
        validate_max_lines "$MAX_RETURN_LINES" || exit_error 1
        shift 2
        ;;
      -s)
        SORT_COLUMN_NAMES="$2"
        validate_sort_col_names "$SORT_COLUMN_NAMES" || exit_error 1
        shift 2
        ;;
      -t)
        FILTER_TAGS="$2"
        validate_tag "$FILTER_TAGS" || exit_error 1
        FILTER_BY_TAG=1
        shift 2
        ;;
      --)
        ACTION="$2"
        validate_action "$ACTION" || exit_error 1
        shift 2
        PARAMS="$@"
        break
        ;;
      *)
        print_error "INTERNAL PROBLEM - Option [$1]"
        usage
        ;;
    esac
  done
}

compute_result() {
  set -o pipefail
  exec_action "$ACTION" "$PARAMS" | filter_tags "$FILTER_TAGS" | filter_max_lines "$MAX_RETURN_LINES" | sort_columns "$SORT_COLUMN_NAMES"
}

read_options_and_parameters "$@"
compute_result
