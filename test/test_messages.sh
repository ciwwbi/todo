#!/bin/bash

set -u

source ./libHelpers
source ../main/lib_message

test_printError_printOnStderr() {

  print_error "DummyMessage" > ${T_STDOUT} 2> ${T_STDERR}

  expected <<-EOE
	ERROR: DummyMessage
	EOE

  assertEquals "stderr contains message" "$expected" "$(cat ${T_STDERR})"
  assertEquals "stdout should be empty" "" "$(cat ${T_STDOUT})"
}

source ${SHUNIT2_INC}/shunit2
