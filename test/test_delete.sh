#!/bin/bash
set -u

source ./libHelpers

test_deleteOneTaskInOneTaskFile_ReturnsOk() {
  
  database <<-EODB
	1	tag	3	3	task
	EODB
 
  expected <<-EODB
	EODB
 
  ${TODO} -d "${database}" delete 1 > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "0" "$?"
  assertStderrEmpty

  typeset effective=$(${TODO} -d "${database}" tasks)
  assertEquals "return value" "${expected}" "${effective}"
}

test_deleteOneTask_ReturnsOk() {
  
  database <<-EODB
	1	tag	3	3	task1
	2	tag	3	3	task2
	5	tag	3	3	task5
	50	tag	3	3	task50
	4	tag	3	3	task4
	EODB
 
  expected <<-EODB
	1	tag	3	3	task1
	2	tag	3	3	task2
	5	tag	3	3	task5
	4	tag	3	3	task4
	EODB
 
  ${TODO} -d "${database}" delete 50 > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "0" "$?"
  assertStderrEmpty

  typeset effective=$(${TODO} -d "${database}" tasks)
  assertEquals "return value" "${expected}" "${effective}"
}

source ${SHUNIT2_INC}/shunit2
