#!/bin/bash
set -u

source ./libHelpers


test_OneTask_ReturnsOneTask() {
  
  database <<-EODB
	1	tag	3	3	task
	EODB
 
  expected <<-EODB
	1	tag	3	3	task
	EODB
 
  typeset effective=$(${TODO} -d "${database}" task 1)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeTasksAskFirst_ReturnsFirstTask() {
  
  database <<-EODB
	1	tag1	3	3	task1
	2	tag2	3	3	task2
	3	tag3	3	3	task3
	EODB

  expected <<-EODB
	1	tag1	3	3	task1
	EODB
 
  typeset effective=$(${TODO} -d "${database}" task 1)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeTasksAskSecond_ReturnsSecondTask() {
  
  database <<-EODB
	1	tag1	3	3	task1
	2	tag2	3	3	task2
	3	tag3	3	3	task3
	EODB
 
  expected <<-EODB
	2	tag2	3	3	task2
	EODB
 
  typeset effective=$(${TODO} -d "${database}" task 2)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeTasksAskThird_ReturnsThirdTask() {
  
  database <<-EODB
	1	tag1	3	3	task1
	2	tag2	3	3	task2
	3	tag3	3	3	task3
	EODB
 
  expected <<-EODB
	3	tag3	3	3	task3
	EODB
 
  typeset effective=$(${TODO} -d "${database}" task 3)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeTasksAskFourth_ReturnsEmpty() {
  
  database <<-EODB
	1	tag1	3	3	task1
	2	tag2	3	3	task2
	3	tag3	3	3	task3
	EODB
 
  typeset effective=$(${TODO} -d "${database}" task 4)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "" "${effective}"
}

test_NoTask_ReturnsEmtpy() {
  
  database <<-EODB
	EODB
 
  typeset effective=$(${TODO} -d "${database}" task 1)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "" "${effective}"
}

source ${SHUNIT2_INC}/shunit2
