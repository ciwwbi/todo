#!/bin/bash
set -u

source ./libHelpers
source ../main/lib_check


test_validateGeNbParameters_oneParameterForOne_returnsOK() {
  validate_ge_nb_parameters 1 1
  assertSuccessOrPrintStderr '1 is greater or equal to 1' "$?"
}

test_validateGeNbParameters_fiveParametersForOne_returnsOK() {
  validate_ge_nb_parameters 1 5
  assertSuccessOrPrintStderr '5 is greater or equal to 1' "$?"
}

test_validateGeNbParameters_zeroParameterForOne_returnsKO() {
  validate_ge_nb_parameters 1 0 > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr '0 is not greater or equal to 1' "$?"
}

test_validateGeNbParameters_zeroParameterForOne_returnsErrorMessage() {
  validate_ge_nb_parameters 1 0 > ${T_STDOUT} 2> ${T_STDERR}
  assertStderrContains "validate message" "INTERNAL ERROR - 1 argument(s) required at least"
}

test_validateGeNbParameters_oneArg_returnsKO() {
  validate_ge_nb_parameters 73 > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'two args required - one is ko' "$?"
}

test_validateGeNbParameters_zeroArg_returnsKO() {
  validate_ge_nb_parameters > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'two args required - zero is ko' "$?"
}

test_validateGeNbParameters_zeroArg_returnsMessage() {
  validate_ge_nb_parameters > ${T_STDOUT} 2> ${T_STDERR}
  assertStderrContains "validate message for number of args" "INTERNAL ERROR - validate_ge_nb_parameters required 2 parameters"
}



test_validateNbParameters_correctNumber_returnsOK() {
  validate_nb_parameters 4 4
  assertSuccessOrPrintStderr '4 parameters is equal to 1' "$?"
}

test_validateNbParameters_uncorrectNumber_returnsKO() {
  validate_nb_parameters 5 4 > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr '5 is not equal to 4 parameters' "$?"
}

test_validateNbParameters_uncorrectNumber_returnsMessage() {
  validate_nb_parameters 18 21 > ${T_STDOUT} 2> ${T_STDERR}
  assertStderrContains "validate message" "INTERNAL ERROR - 18 argument(s) required"
}

test_validateNbParameters_oneArg_returnsKO() {
  validate_nb_parameters 73 > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'two args required - one is ko' "$?"
}

test_validateNbParameters_zeroArg_returnsKO() {
  validate_nb_parameters > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'two args required - zero is ko' "$?"
}

test_validateNbParameters_zeroArg_returnsMessage() {
  validate_nb_parameters > ${T_STDOUT} 2> ${T_STDERR}
  assertStderrContains "validate message for number of args" "INTERNAL ERROR - validate_nb_parameters required 2 parameters"
}



test_testPositiveInteger_positiveInteger_returnsOK() {
  test_positive_integer 42
  assertSuccessOrPrintStderr '42 is a positive integer' "$?"
}

test_testPositiveInteger_string_returnsKO() {
  test_positive_integer abc
  assertFailOrPrintStderr 'abc is not an integer' "$?"
}

test_testPositiveInteger_negativeInteger_returnsKO() {
  test_positive_integer -2
  assertFailOrPrintStderr '-2 is not a positive integer' "$?"
}

test_testPositiveInteger_noArg_returnsKO() {
  test_positive_integer > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'No arg forbidden' $?
}



test_validateTaskId_positiveInteger_returnsOK() {
  validate_task_id 234 > ${T_STDOUT} 2> ${T_STDERR}
  assertSuccessOrPrintStderr '234 is a valid task id' $?
  assertStderrEmpty
}

test_validateTaskId_string_returnsKO() {
  validate_task_id abc > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'abc is not a valid task id' $?
  assertStderrContains "Not integer arg message" "TASK_ID [abc] should be a positive integer"
}

test_validateTaskId_emptyArg_returnsKO() {
  validate_task_id '' > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'Empty arg is not a valid task id' $?
  assertStderrContains "Empty arg message" "TASK_ID undefined"
}

test_validateTaskId_noArg_returnsKO() {
  validate_task_id > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'No arg forbidden' $?
}




test_validateExistingTaskId_idExisting_returnsOK() {
  database <<-EODB
	234	tag	3	3	task
	EODB

  validate_existing_task_id 234 "${database}" > ${T_STDOUT} 2> ${T_STDERR}
  assertSuccessOrPrintStderr '234 is a valid task id' $?
  assertStderrEmpty
}

test_validateExistingTaskId_idNotPresent_returnsKO() {
  database <<-EODB
	1	tag	3	3	task
	EODB

  validate_existing_task_id abc ${database} > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'abc is not an existing task id' $?
  assertStderrContains "Not present message" "TASK_ID [abc] not present in [${database}]"
}

test_validateExistingTaskId_emptyArg_returnsKO() {
  database <<-EODB
	1	tag	3	3	task
	EODB

  validate_existing_task_id '' ${database} > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'Empty arg is not a valid task id' $?
  assertStderrContains "Not present message" "TASK_ID undefined"
}

test_validateExistingTaskId_noArg_returnsKO() {
  database <<-EODB
	1	tag	3	3	task
	EODB

  validate_existing_task_id ${database} > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'No arg forbidden' $?
}



test_validateMaxLines_positiveInteger_returnsOK() {
  validate_max_lines 2 > ${T_STDOUT} 2> ${T_STDERR}
  assertSuccessOrPrintStderr '2 is a valid max number of lines' $?
  assertStderrEmpty
}

test_validateMaxLines_negativeInteger_returnsKO() {
  validate_max_lines -3 > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'negative is not a valid max number of lines' $?
  assertStderrContains "negative integer arg message" "MAX_RETURN_LINES [-3] should be a positive integer"
}

test_validateMaxLines_string_returnsKO() {
  validate_max_lines abc > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'abc is not a valid max number of lines' $?
  assertStderrContains "Not integer arg message" "MAX_RETURN_LINES [abc] should be a positive integer"
}

test_validateMaxLines_emptyArg_returnsKO() {
  validate_max_lines '' > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'Empty arg is not a valid max number of lines' $?
  assertStderrContains "Empty arg message" "MAX_RETURN_LINES undefined"
}

test_validateMaxLines_noArg_returnsKO() {
  validate_max_lines > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'No arg forbidden' $?
}



test_validateAction_tags_returnsOK() {
  validate_action tags > ${T_STDOUT} 2> ${T_STDERR}
  assertSuccessOrPrintStderr 'tags is a valid action' $?
  assertStderrEmpty
}

test_validateAction_tasks_returnsOK() {
  validate_action tasks > ${T_STDOUT} 2> ${T_STDERR}
  assertSuccessOrPrintStderr 'tasks is a valid action' $?
  assertStderrEmpty
}

test_validateAction_task_returnsOK() {
  validate_action task > ${T_STDOUT} 2> ${T_STDERR}
  assertSuccessOrPrintStderr 'task is a valid action' $?
  assertStderrEmpty
}

test_validateAction_create_returnsOK() {
  validate_action create > ${T_STDOUT} 2> ${T_STDERR}
  assertSuccessOrPrintStderr 'create is a valid action' $?
  assertStderrEmpty
}

test_validateAction_unknown_returnsKO() {
  validate_action unknown > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'unknown is not a valid action' $?
  assertStderrContains "unknown action" "ACTION [unknown] unknown"
}

test_validateAction_emptyString_returnsKO() {
  validate_action '' > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'empty string is not a valid action' $?
  assertStderrContains "Undefined action" "ACTION undefined"
}

test_validateAction_noArg_returnsKO() {
  validate_action > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'no arg forbidden' $?
}



test_validateSortColNames_prio_returnsOK() {
  validate_sort_col_names prio > ${T_STDOUT} 2> ${T_STDERR}
  assertSuccessOrPrintStderr 'prio is a valid col name' $?
  assertStderrEmpty
}

test_validateSortColNames_value_returnsOK() {
  validate_sort_col_names value > ${T_STDOUT} 2> ${T_STDERR}
  assertSuccessOrPrintStderr 'value is a valid col name' $?
  assertStderrEmpty
}

test_validateSortColNames_prioValue_returnsOK() {
  validate_sort_col_names "prio,value" > ${T_STDOUT} 2> ${T_STDERR}
  assertSuccessOrPrintStderr 'prio,value are valid col names' $?
  assertStderrEmpty
}

test_validateSortColNames_valuePrio_returnsOK() {
  validate_sort_col_names "value,prio" > ${T_STDOUT} 2> ${T_STDERR}
  assertSuccessOrPrintStderr 'value,prio are valid col names' $?
  assertStderrEmpty
}

test_validateSortColNames_unknown_returnsKO() {
  validate_sort_col_names unknown > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'unknown is not a valid col name' $?
  assertStderrContains "unknown col name" "COLUMN [unknown] not in (prio, value)"
}

test_validateSortColNames_unknownInList_returnsKO() {
  validate_sort_col_names "prio,unknown,value" > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'unknown is not a valid col name' $?
  assertStderrContains "unknown col name" "COLUMN [unknown] not in (prio, value)"
}

test_validateSortColNames_emptyString_returnsKO() {
  validate_sort_col_names '' > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'empty string is not a valid col name' $?
  assertStderrContains "empty col name" "COLUMN [] not in (prio, value)"
}

test_validateSortColNames_noArg_returnsKO() {
  validate_sort_col_names > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'no arg forbidden' $?
}



test_validateTag_emptyString_returnsOK() {
  validate_tag '' > ${T_STDOUT} 2> ${T_STDERR}
  assertSuccessOrPrintStderr 'tag can be empty' $?
  assertStderrEmpty
}

test_validateTag_string_returnsOK() {
  validate_tag "dummy" > ${T_STDOUT} 2> ${T_STDERR}
  assertSuccessOrPrintStderr 'string is a valid tag' $?
  assertStderrEmpty
}

test_validateTag_unescapedOpeningBracket_returnsKO() {
  validate_tag 'du(mmy' > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'unescaped bracket is not valid' $?
  assertStderrContains "unescaped special char message" "Value '([/' should be escaped in tags"
}

test_validateTag_unescapedOpeningSquareBracket_returnsKO() {
  validate_tag 'du[mmy' > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'unescaped squared bracket is not valid' $?
  assertStderrContains "unescaped special char message" "Value '([/' should be escaped in tags"
}

test_validateTag_unescapedSlash_returnsKO() {
  validate_tag 'dum/my' > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'unescaped slash is not valid' $?
  assertStderrContains "unescaped special char message" "Value '([/' should be escaped in tags"
}

test_validateTag_noArg_returnsKO() {
  validate_tag > ${T_STDOUT} 2> ${T_STDERR}
  assertFailOrPrintStderr 'no arg forbidden' $?
}



test_validateAddParam_oneParam_returnsOK() {
  $(validate_add_param_or_exit 'abc' > ${T_STDOUT} 2> ${T_STDERR} )
  assertSuccessOrPrintStderr 'One word task name' $?
}

test_validateAddParam_fiveParam_returnsOK() {
  $(validate_add_param_or_exit 'abc' def "ghi" 12 vb > ${T_STDOUT} 2> ${T_STDERR} )
  assertSuccessOrPrintStderr 'One word task name' $?
}

test_validateAddParam_emptyString_returnsKO() {
  $(validate_add_param_or_exit '' > ${T_STDOUT} 2> ${T_STDERR} )
  assertFailOrPrintStderr 'Task name cannot be empty' $?
  assertStderrContains "Empty task name message" "Name of task should be defined"
}

test_validateAddParam_noParam_returnsKO() {
  $(validate_add_param_or_exit > ${T_STDOUT} 2> ${T_STDERR} )
  assertFailOrPrintStderr 'Task name should be defined' $?
  assertStderrContains "Empty task name message" "Name of task should be defined"
}


source ${SHUNIT2_INC}/shunit2
