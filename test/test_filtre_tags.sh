#!/bin/bash
set -u

source ./libHelpers

test_TwoLinesTwoTags_FilterOneTag_ReturnsOneLine() {

  database <<-EODB
	1	tag	3	3	task1
	2	othertag	1	2	task2
	EODB
 
  expected <<-EOE
	1	tag	3	3	task1
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -t tag tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_TwoLinesTwoTagsWithSpecialChar_FilterOneTag_ReturnsOneLine() {

  database <<-EODB
	1	tag	3	3	task1
	2	other\$tag	1	2	task2
	EODB
 
  expected <<-EOE
	2	other\$tag	1	2	task2
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -t 'other\$tag' tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeLinesTwoTags_FilterOneTag_ReturnsTwoLines() {

  database <<-EODB
	1	tag	3	3	task1
	2	tags	3	3	task2
	3	tag	3	3	task3
	EODB
 
  expected <<-EOE
	1	tag	3	3	task1
	3	tag	3	3	task3
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -t tag tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeLines_FilterEmptyTag_ReturnsOneLine() {

  database <<-EODB
	1	dummy	3	3	task1
	2	dummy	3	3	task2
	3		3	3	task3
	EODB
 
  expected <<-EOE
	3		3	3	task3
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -t ''  tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeLinesTwoTags_FilterUnknownTag_ReturnsZeroLines() {

  database <<-EODB
	1	tag	3	3	task1
	2	tags	3	3	task2
	3	tag	3	3	task3
	EODB
 
  expected <<-EOE
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -t unknown tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeLinesTwoTagsOnOneLine_FilterOneTag_ReturnsOneLine() {

  database <<-EODB
	1	tag,dummy	3	3	task1
	2	dummy1	3	3	task2
	3	dummy2	3	3	task3
	EODB
 
  expected <<-EOE
	1	tag,dummy	3	3	task1
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -t tag tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeLinesThreeTagsOnOneLine_FilterOneTag_ReturnsOneLine() {

  database <<-EODB
	1	dummy0,tag,dummy	3	3	task1
	2	dummy1	3	3	task2
	3	dummy2	3	3	task3
	EODB
 
  expected <<-EOE
	1	dummy0,tag,dummy	3	3	task1
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -t tag tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeLinesOfOneTag_FilterTwoTags_ReturnsEmpty() {

  database <<-EODB
	1	tag1	3	3	task1
	2	tag2	3	3	task2
	3	tag3	3	3	task3
	EODB
 
  expected <<-EOE
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -t tag1,tag2 tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeLinesOfTwoTag_FilterTwoTags_ReturnsOneLine() {

  database <<-EODB
	1	tag1,tag2	3	3	task1
	2	tag2,tag3	3	3	task2
	3	tag3,tag2	3	3	task3
	EODB
 
  expected <<-EOE
	1	tag1,tag2	3	3	task1
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -t tag1,tag2 tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

source ${SHUNIT2_INC}/shunit2
