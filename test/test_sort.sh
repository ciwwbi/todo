#!/bin/bash
set -u

source ./libHelpers

test_sortPrioForSortedTasks_ReturnsSameSortedTasks() {

  database <<-EODB
	1	tag1	1	3	task1
	2	tag2	1	3	task2
	3	tag3	2	3	task3
	4	tag2	2	3	task4
	5	tag1	3	3	task5
	EODB
 
  expected <<-EOE
	1	tag1	1	3	task1
	2	tag2	1	3	task2
	3	tag3	2	3	task3
	4	tag2	2	3	task4
	5	tag1	3	3	task5
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -s prio tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_sortPrioForUnsortedTasks_ReturnsSortedTasks() {

  database <<-EODB
	1	tag1	1	3	task1
	2	tag2	2	3	task2
	3	tag3	3	3	task3
	4	tag2	1	3	task4
	5	tag1	2	3	task5
	EODB
 
  expected <<-EOE
	1	tag1	1	3	task1
	4	tag2	1	3	task4
	2	tag2	2	3	task2
	5	tag1	2	3	task5
	3	tag3	3	3	task3
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -s prio tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_sortValueForSortedTasks_ReturnsSameSortedTasks() {

  database <<-EODB
	1	tag1	3	1	task1
	2	tag2	2	1	task2
	3	tag3	1	2	task3
	4	tag2	2	3	task4
	5	tag1	3	3	task5
	EODB
 
  expected <<-EOE
	1	tag1	3	1	task1
	2	tag2	2	1	task2
	3	tag3	1	2	task3
	4	tag2	2	3	task4
	5	tag1	3	3	task5
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -s value tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_sortValueForUnsortedTasks_ReturnsSortedTasks() {

  database <<-EODB
	1	tag1	3	1	task1
	2	tag2	2	2	task2
	3	tag3	1	3	task3
	4	tag2	2	2	task4
	5	tag1	3	3	task5
	EODB
 
  expected <<-EOE
	1	tag1	3	1	task1
	2	tag2	2	2	task2
	4	tag2	2	2	task4
	3	tag3	1	3	task3
	5	tag1	3	3	task5
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -s value tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_sortPrioValueForSortedTasks_ReturnsSameSortedTasks() {

  database <<-EODB
	1	tag1	1	1	task1
	2	tag2	2	1	task2
	3	tag3	2	2	task3
	4	tag2	3	3	task4
	5	tag1	3	3	task5
	EODB
 
  expected <<-EOE
	1	tag1	1	1	task1
	2	tag2	2	1	task2
	3	tag3	2	2	task3
	4	tag2	3	3	task4
	5	tag1	3	3	task5
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -s prio,value tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_sortPrioValueForUnsortedTasks_ReturnsSortedTasks() {

  database <<-EODB
	1	tag1	3	1	task1
	2	tag2	2	2	task2
	3	tag3	1	3	task3
	4	tag2	2	1	task4
	5	tag1	3	3	task5
	EODB
 
  expected <<-EOE
	3	tag3	1	3	task3
	4	tag2	2	1	task4
	2	tag2	2	2	task2
	1	tag1	3	1	task1
	5	tag1	3	3	task5
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -s prio,value tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_sortValuePrioForSortedTasks_ReturnsSameSortedTasks() {

  database <<-EODB
	1	tag1	1	1	task1
	2	tag2	2	1	task2
	3	tag3	2	2	task3
	4	tag2	3	3	task4
	5	tag1	3	3	task5
	EODB
 
  expected <<-EOE
	1	tag1	1	1	task1
	2	tag2	2	1	task2
	3	tag3	2	2	task3
	4	tag2	3	3	task4
	5	tag1	3	3	task5
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -s value,prio tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_sortValuePrioForUnsortedTasks_ReturnsSortedTasks() {

  database <<-EODB
	1	tag1	3	1	task1
	2	tag2	2	2	task2
	3	tag3	1	3	task3
	4	tag2	2	1	task4
	5	tag1	3	3	task5
	EODB
 
  expected <<-EOE
	4	tag2	2	1	task4
	1	tag1	3	1	task1
	2	tag2	2	2	task2
	3	tag3	1	3	task3
	5	tag1	3	3	task5
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -s value,prio tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

source ${SHUNIT2_INC}/shunit2
