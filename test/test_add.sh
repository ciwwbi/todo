#!/bin/bash
set -u

source ./libHelpers

test_addOneTaskWithEmptyDB_ReturnsOK() {
  
  database <<-EODB
	EODB
 
  expected <<-EODB
	1		3	3	dummy
	EODB
 
  ${TODO} -d "${database}" add dummy > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "0" "$?"
  assertEquals "no errors" "" "$(cat ${T_STDERR})"

  typeset effective=$(${TODO} -d "${database}" tasks | tail -n 1 )
  assertEquals "return value" "${expected}" "${effective}"
}

test_addOneTaskWithQuotes_ReturnsOK() {
  
  database <<-EODB
	EODB
 
  expected <<-EODB
	1		3	3	dummy name
	EODB
 
  ${TODO} -d "${database}" add "dummy name" > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "0" "$?"
  assertEquals "no errors" "" "$(cat ${T_STDERR})"

  typeset effective=$(${TODO} -d "${database}" tasks | tail -n 1 )
  assertEquals "return value" "${expected}" "${effective}"
}

test_addOneTaskWithoutQuotes_ReturnsOK() {
  
  database <<-EODB
	EODB
 
  expected <<-EODB
	1		3	3	dummy name
	EODB
 
  ${TODO} -d "${database}" add dummy name > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "0" "$?"
  assertEquals "no errors" "" "$(cat ${T_STDERR})"

  typeset effective=$(${TODO} -d "${database}" tasks | tail -n 1 )
  assertEquals "return value" "${expected}" "${effective}"
}

test_addOneTaskWithQuotesAndTag_ReturnsOK() {
  
  database <<-EODB
	1	tag	3	3	task
	EODB
 
  expected <<-EODB
	2	newtask	3	3	task just added
	EODB
 
  ${TODO} -d "${database}" add "task just added" @newtask > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "0" "$?"

  typeset effective=$(${TODO} -d "${database}" tasks | tail -n 1 )
  assertEquals "return value" "${expected}" "${effective}"
}

test_addOneTaskWithoutQuotesAndWithTag_ReturnsOK() {
  
  database <<-EODB
	1	tag	3	3	task
	EODB
 
  expected <<-EODB
	2	newtask	3	3	task just added
	EODB
 
  ${TODO} -d "${database}" add task just added @newtask > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "0" "$?"

  typeset effective=$(${TODO} -d "${database}" tasks | tail -n 1 )
  assertEquals "return value" "${expected}" "${effective}"
}

test_addOneTaskWithoutQuotesAndWithMultipleTag_ReturnsOK() {
  
  database <<-EODB
	1	tag	3	3	task
	EODB
 
  expected <<-EODB
	2	tag1,tag2	3	3	name
	EODB
 
  ${TODO} -d "${database}" add name @tag1 @tag2 > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "0" "$?"

  typeset effective=$(${TODO} -d "${database}" tasks | tail -n 1 )
  assertEquals "return value" "${expected}" "${effective}"
}

test_addOneTaskWithIdGreaterThan10_ReturnsOK() {
  
  database <<-EODB
	1	T1	1	1	Tache1
	2	T1	1	1	Tache2
	3	T1	1	1	Tache3
	4	T1	1	1	Tache4
	5	T1	1	1	Tache5
	6	T1	1	1	Tache6
	7	T1	1	1	Tache7
	8	T1	1	1	Tache8
	9	T1	1	1	Tache9
	10	T1	1	1	Tache10
	EODB
 
  expected <<-EODB
	11		3	3	task just added
	EODB
 
  ${TODO} -d "${database}" add "task just added" > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "0" "$?"

  typeset effective=$(${TODO} -d "${database}" tasks | tail -n 1 )
  assertEquals "return value" "${expected}" "${effective}"
}

test_addWithoutTask_ReturnsKO() {
  
  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" add > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "1" "$?"
}

source ${SHUNIT2_INC}/shunit2
