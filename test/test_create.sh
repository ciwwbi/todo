#!/bin/bash
set -u

source ./libHelpers

test_createOneTask_ReturnsOk() {
  
  database <<-EODB
	1	tag	3	3	task
	EODB
 
  expected <<-EODB
	2	newtask	3	3	task just created
	EODB
 
  ${TODO} -d "${database}" create "newtask	3	3	task just created" > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "0" "$?"

  typeset effective=$(${TODO} -d "${database}" tasks | tail -n 1 )
  assertEquals "return value" "${expected}" "${effective}"
}

test_createOneTaskWithIdGreaterThan10_ReturnsOk() {
  
  database <<-EODB
	1	T1	1	1	Tache1
	2	T1	1	1	Tache2
	3	T1	1	1	Tache3
	4	T1	1	1	Tache4
	5	T1	1	1	Tache5
	6	T1	1	1	Tache6
	7	T1	1	1	Tache7
	8	T1	1	1	Tache8
	9	T1	1	1	Tache9
	10	T1	1	1	Tache10
	EODB
 
  expected <<-EODB
	11	newtask	3	3	task just created
	EODB
 
  ${TODO} -d "${database}" create "newtask	3	3	task just created" > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "0" "$?"

  typeset effective=$(${TODO} -d "${database}" tasks | tail -n 1 )
  assertEquals "return value" "${expected}" "${effective}"
}

test_createOneTaskWithEmptyDB_ReturnsOk() {
  
  database <<-EODB
	EODB
 
  expected <<-EODB
	1	dummy	3	3	dummy
	EODB
 
  ${TODO} -d "${database}" create "dummy	3	3	dummy" > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "0" "$?"
  assertEquals "no errors" "" "$(cat ${T_STDERR})"

  typeset effective=$(${TODO} -d "${database}" tasks | tail -n 1 )
  assertEquals "return value" "${expected}" "${effective}"
}

test_createOneTaskWithNoTag_ReturnsOk() {
  
  database <<-EODB
	EODB
 
  expected <<-EODB
	1		3	3	dummy name
	EODB
 
  ${TODO} -d "${database}" create "	3	3	dummy name" > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "0" "$?"
  assertEquals "no errors" "" "$(cat ${T_STDERR})"

  typeset effective=$(${TODO} -d "${database}" tasks | tail -n 1 )
  assertEquals "return value" "${expected}" "${effective}"
}

test_createOneTaskWithNoTabs_RetursMessage() {
  
  database <<-EODB
	EODB
 
  ${TODO} -d "$database" create "newtask 1 1 task" > ${T_STDOUT} 2> ${T_STDERR}
  assertEquals "return code" "1" "$?"
  assertStderrContains "error message" "Not enough values to create task"

}

test_createOneTask_PrioNotAnInteger_ReturnsMessage() {
  
  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" create "newtask	abc	3	task just created" > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "1" "$?"
  assertStderrContains "error message" "Priority value should be a positive integer" 
}

test_createOneTask_PrioLessThan1_ReturnsMessage() {
  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" create "newtask	0	3	task just created" > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "1" "$?"
  assertStderrContains "error message" "Priority value should be in [1,3]" 
}

test_createOneTask_PrioMoreThan3_ReturnsMessage() {
  
  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" create "newtask	5	3	task just created" > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "1" "$?"
  assertStderrContains "error message" "Priority value should be in [1,3]" 
}

test_createOneTask_ValueNotAnInteger_ReturnsMessage() {
  
  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" create "newtask	3	gllkfdjh	task just created" > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "1" "$?"
  assertStderrContains "error message" "Task value should be a positive integer" 
}

test_createOneTask_ValueLessThan1_ReturnsMessage() {
  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" create "newtask	3	0	task just created" > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "1" "$?"
  assertStderrContains "error message" "Task value should be in [1,3]" 
}

test_createOneTask_ValueMoreThan3_ReturnsMessage() {
  
  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" create "newtask	3	5	task just created" > ${T_STDOUT} 2> ${T_STDERR} 
  assertEquals "return code" "1" "$?"
  assertStderrContains "error message" "Task value should be in [1,3]" 
}

source ${SHUNIT2_INC}/shunit2
