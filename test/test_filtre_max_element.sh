#!/bin/bash
set -u

source ./libHelpers

test_ThreeLines_AskTwoFirst_ReturnsTwo() {

  database <<-EODB
	1	tag	3	3	task1
	2	tags	3	3	task2
	3	tag	3	3	task3
	EODB
 
  expected <<-EOE
	1	tag	3	3	task1
	2	tags	3	3	task2
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -n 2 tasks)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeLines_AskFourFirst_ReturnsThree() {

  database <<-EODB
	1	tag	3	3	task1
	2	tags	3	3	task2
	3	tag	3	3	task3
	EODB
 
  expected <<-EOE
	1	tag	3	3	task1
	2	tags	3	3	task2
	3	tag	3	3	task3
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -n 4 tasks)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeLines_AskZero_ReturnsEmpty() {

  database <<-EODB
	1	tag	3	3	task1
	2	tags	3	3	task2
	3	tag	3	3	task3
	EODB
 
  expected <<-EOE
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -n 0 tasks)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ZeroLine_AskZero_ReturnsEmpty() {

  database <<-EODB
	EODB
 
  expected <<-EOE
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -n 0 tasks)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ZeroLine_AskThree_ReturnsEmpty() {

  database <<-EODB
	EODB
 
  expected <<-EOE
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -n 3 tasks)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeTags_AskTwoFirst_ReturnsTwo() {

  database <<-EODB
	1	tag1	3	3	task1
	2	tag2	3	3	task2
	3	tag3	3	3	task3
	EODB
 
  expected <<-EOE
	tag1
	tag2
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -n 2 tags)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeTags_AskFourFirst_ReturnsThree() {

  database <<-EODB
	1	tag1	3	3	task1
	2	tag2	3	3	task2
	3	tag3	3	3	task3
	EODB
 
  expected <<-EOE
	tag1
	tag2
	tag3
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -n 4 tags)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeTags_AskZero_ReturnsEmpty() {

  database <<-EODB
	1	tag1	3	3	task1
	2	tag2	3	3	task2
	3	tag3	3	3	task3
	EODB
 
  expected <<-EOE
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -n 0 tags)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ZeroTags_AskTwoFirst_ReturnsEmpty() {

  database <<-EODB
	1		3	3	task1
	2		3	3	task2
	3		3	3	task3
	EODB
 
  expected <<-EOE
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -n 4 tags)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ZeroLines_AskTwoTags_ReturnsEmpty() {

  database <<-EODB
	EODB
 
  expected <<-EOE
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -n 2 tags)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

source ${SHUNIT2_INC}/shunit2
