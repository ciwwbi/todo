#!/bin/bash

set -u

./test_messages.sh

./test_cli.sh

./test_checks.sh

./test_liste_taches.sh
./test_liste_tags.sh
./test_tache.sh

./test_filtre_max_element.sh
./test_filtre_tags.sh
./test_filtre_combined.sh

./test_sort.sh

./test_create.sh
./test_add.sh
./test_delete.sh
