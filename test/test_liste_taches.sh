#!/bin/bash
set -u

source ./libHelpers

test_OneTask_ReturnsOneTask() {

  database <<-EODB
	1	tag	3	3	task
	EODB
 
  expected <<-EOE
	1	tag	3	3	task
	EOE
 
  typeset effective=$(${TODO} -d "${database}" tasks)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_ThreeTasks_ReturnsAllTAsks() {

  database <<-EODB
	1	tag	3	3	task t1
	2	tags	3	3	task2 t2
	3	tag	3	3	task3
	EODB
 
  expected <<-EOE
	1	tag	3	3	task t1
	2	tags	3	3	task2 t2
	3	tag	3	3	task3
	EOE
 
  typeset effective=$(${TODO} -d "${database}" tasks)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_NoTask_ReturnsEmtpy() {

  database <<-EODB
	EODB
 
  typeset effective=$(${TODO} -d "${database}" tasks)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "" "${effective}"
}

test_dbWithBadLines_ReturnsOnlyCorrectLines() {
  database <<-EODB
	1	tag	3	3	task t1
	45	titi	task
	2	tags	3	3	task2
	3	tag	3	3	task3

	EODB
 
  expected <<-EOE
	1	tag	3	3	task t1
	2	tags	3	3	task2
	3	tag	3	3	task3
	EOE
 
  typeset effective=$(${TODO} -d "${database}" tasks)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

source ${SHUNIT2_INC}/shunit2
