#!/bin/bash
set -u

source ./libHelpers

test_OneTaskOneTag_ReturnsItsTag() {

  database <<-EODB  
	1	tag	3	3	task
	EODB
 
  typeset effective=$(${TODO} -d "${database}" tags)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "tag" "${effective}"
}


test_TwoTasksTwoTags_ReturnsTwoTags() {
  
  database <<-EODB  
	1	tag1	3	3	task
	2	tag2	3	3	task2
	EODB
 
  expected <<-EOE
	tag1
	tag2
	EOE

  typeset effective=$(${TODO} -d "${database}" tags)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_TwoTasksOneTag_ReturnsTag() {
  database <<-EODB  
	1	tag	3	3	task
	2	tag	3	3	task2
	EODB
 
  typeset effective=$(${TODO} -d "${database}" tags)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "tag" "${effective}"
}

test_TwoTasksThreeTags_ReturnsThreeTags() {
  database <<-EODB  
	1	tag1,tag3	3	3	task
	2	tag2	3	3	task2
	EODB

  expected <<-EOE 
	tag1
	tag2
	tag3
	EOE
  
  typeset effective=$(${TODO} -d "${database}" tags)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_TwoTasksFourTags_ReturnsThreeTags() {
  database <<-EODB  
	1	tag1,tag3	3	3	task
	2	tag2,tag1	3	3	task2
	EODB
 
  expected <<-EOE 
	tag1
	tag2
	tag3
	EOE
  
  typeset effective=$(${TODO} -d "${database}" tags)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

test_OneTaskNoTag_ReturnsEmpty() {
  database <<-EODB  
	1		3	3	task
	EODB
 
  typeset effective=$(${TODO} -d "${database}" tags)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "" "${effective}"
}

test_TwoTasksNoTag_ReturnsEmpty() {
  database <<-EODB  
	1		3	3	task
	2		3	3	task2
	EODB
 
  typeset effective=$(${TODO} -d "${database}" tags)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "" "${effective}"
}

test_TwoTasksOneEmptyTag_ReturnsOneTagAndNoEmptyLine() {
  database <<-EODB  
	1	tag	3	3	task
	2		3	3	task2
	EODB

  typeset effective=$(${TODO} -d "${database}" tags)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "tag" "${effective}"
}

test_EmptyDB_ReturnsEmpty() {
  database <<-EODB  
	EODB

  typeset effective=$(${TODO} -d "${database}" tags)

  assertEquals "return code" "0" "$?"
  assertEquals "return value" "" "${effective}"
}

source ${SHUNIT2_INC}/shunit2
