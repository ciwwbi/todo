#!/bin/bash

set -u

source ./libHelpers

TODO_BASENAME=$(basename ${TODO})

test_optionH_ReturnsUsage() {

  ${TODO} -H > ${T_STDOUT} 2> ${T_STDERR}

  assertSuccessOrPrintStderr "returns success code" "$?"
  assertStderrContains "return usage" "usage: ${TODO_BASENAME}"
}

test_optionh_ReturnsUsage() {

  ${TODO} -h > ${T_STDOUT} 2> ${T_STDERR}

  assertSuccessOrPrintStderr "returns success code" "$?"
  assertStderrContains "return usage" "usage: ${TODO_BASENAME}"
}

test_optionUnknown_ReturnsMessage() {

  ( LANG=fr_FR ; ${TODO} -z > ${T_STDOUT} 2> ${T_STDERR} )

  assertFailOrPrintStderr "return code" "1" "$?"
  assertStderrContains "unknwon option name"  'invalid option'
  assertStderrContains "return usage" "For more information, try : ${TODO_BASENAME} -h"
  
}

test_optionDbFileUnknown_ReturnsMessage() {

  ${TODO} -d "/unknown/file" tasks > ${T_STDOUT} 2> ${T_STDERR}

  assertFailOrPrintStderr "return code" "1" "$?"
  assertStderrContains "return error"  '/unknown/file'
}

test_optionDbFileMissing_ReturnsMessage() {

  ( LANG=fr_FR ; ${TODO} -d > ${T_STDOUT} 2> ${T_STDERR} )

  assertFailOrPrintStderr "return code" "1" "$?"
  assertStderrContains "db file missing" "option requires an argument"
  assertStderrContains "return usage" "For more information, try : ${TODO_BASENAME} -h"
}

test_filterMaxElementsNotInteger_ReturnsMessage() {
  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" -n "dummy" tasks > ${T_STDOUT} 2> ${T_STDERR}

  assertFailOrPrintStderr "return code" "1" "$?"
  assertStderrContains "return error"  'dummy'
}

test_filterMaxElementsWithoutValue_ReturnsMessage() {

  database <<-EODB
	EODB
 
  ( LANG=fr_FR ; ${TODO} -d "${database}" tasks -n > ${T_STDOUT} 2> ${T_STDERR} )

  assertFailOrPrintStderr "return code" "1" "$?"
  assertStderrContains "max elements value missing" "option requires an argument"
  assertStderrContains "return usage" "For more information, try : ${TODO_BASENAME} -h"
}

test_filterTagWithoutValue_ReturnsMessage() {
  database <<-EODB
	EODB
 
  ( LANG=fr_FR ; ${TODO} -d "${database}" tasks -t > ${T_STDOUT} 2> ${T_STDERR} )

  assertFailOrPrintStderr "return code" "1" "$?"
  assertStderrContains "tag filter value missing" "option requires an argument"
  assertStderrContains "return usage" "For more information, try : ${TODO_BASENAME} -h"
}

test_filterTagWithParenthesis_ReturnsMessage() {
  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" tasks -t 'du(m' > ${T_STDOUT} 2> ${T_STDERR}

  assertFailOrPrintStderr "return code" "1" "$?"
  assertStderrContains "return error" "Value '([/' should be escaped in tags"
}

test_filterTagWithSquareBracket_ReturnsMessage() {
  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" tasks -t 'du[m' > ${T_STDOUT} 2> ${T_STDERR}

  assertFailOrPrintStderr "return code" "1" "$?"
  assertStderrContains "return error" "Value '([/' should be escaped in tags"
}

test_filterTagWithSlash_ReturnsMessage() {
  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" tasks -t 'du/m' > ${T_STDOUT} 2> ${T_STDERR}

  assertFailOrPrintStderr "return code" "1" "$?"
  assertStderrContains "return error" "Value '([/' should be escaped in tags"
}

test_sortWithoutValue_ReturnsMessage() {
  database <<-EODB
	EODB
 
  ( LANG=fr_FR ; ${TODO} -d "${database}" tasks -s > ${T_STDOUT} 2> ${T_STDERR} )

  assertFailOrPrintStderr "return code" "1" "$?"
  assertStderrContains "sort value missing" "option requires an argument"
  assertStderrContains "return usage" "For more information, try : ${TODO_BASENAME} -h"
}

test_sortWithBadValue_ReturnsMessage() {
  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" tasks -s bad_value > ${T_STDOUT} 2> ${T_STDERR}

  assertFailOrPrintStderr "return code" "1" "$?"
  assertStderrContains "bad sort value" "bad_value"
}

test_actionUnknown_ReturnsMessage() {

  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" action_unknown > ${T_STDOUT} 2> ${T_STDERR}

  assertFailOrPrintStderr "return code" "1" "$?"
  assertStderrContains "unknown action name" "action_unknown"
}

test_actionMissing_ReturnsMessage() {

  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" > ${T_STDOUT} 2> ${T_STDERR}

  assertFailOrPrintStderr "return code" "1" "$?"
  assertStderrContains "action missing" "ACTION undefined"
}

test_actionTaskWithoutArgument_ReturnsMessage() {

  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" task > ${T_STDOUT} 2> ${T_STDERR}

  assertFailOrPrintStderr "return code" "1" "$?"
  assertStderrContains "return usage" "For more information, try : ${TODO_BASENAME} -h"
}

test_actionKnown_ReturnsOk() {
  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" tags > ${T_STDOUT} 2> ${T_STDERR}

  assertSuccessOrPrintStderr "return code" "$?"
  assertStderrEmpty
}

test_ok_stderrEmpty() {

  database <<-EODB
	EODB
 
  ${TODO} -d "${database}" tasks > ${T_STDOUT} 2> ${T_STDERR}

  assertSuccessOrPrintStderr "return code" "$?"
  assertStderrEmpty
}

source ${SHUNIT2_INC}/shunit2
