#!/bin/bash
set -u

source ./libHelpers

test_FilterTagAndMaxLine_ReturnsMaxLine() {

  database <<-EODB
	1	tag1,tag2	3	3	task1
	2	tag2,tag3	3	3	task2
	3	tag3,tag2	3	3	task3
	4	tag2,tag3	3	3	task4
	5	tag1,tag2	3	3	task5
	EODB
 
  expected <<-EOE
	2	tag2,tag3	3	3	task2
	3	tag3,tag2	3	3	task3
	EOE
  
  typeset effective=$(${TODO} -d "${database}" -n 2 -t tag2,tag3 tasks 2>"${T_STDERR}")

  assertSuccessOrPrintStderr "return code" "$?"
  assertEquals "return value" "${expected}" "${effective}"
}

source ${SHUNIT2_INC}/shunit2
